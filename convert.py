import json

c = dict()
d = json.loads(open('natives.json').read().decode('utf-8-sig'))
for category in d:
    for hexentry in d[category]:
        name = str(d[category][hexentry]["name"])
        results = str(d[category][hexentry]["results"])
        hexentry = str(hexentry)
        params = d[category][hexentry]["params"]
        
        if name != "":
            name = name.replace("_", " ").title().replace(" ", "")
            
            c[hexentry] = dict()
            c[hexentry]["prefix"] = name
            c[hexentry]["description"] = results
            
            p1 = "("
            p2 = "("
            for param in params:
                p1 += param["type"] + " " + param["name"] + ", "
                p2 += param["name"] + ", "
            p1 = p1[:-2] + ")"
            p2 = p2[:-2] + ")"
            
            c[hexentry]["prefix"] += p1
            c[hexentry]["body"] = name + p2
            
with open('lua.json', 'w') as out:
    json.dump(c, out)
